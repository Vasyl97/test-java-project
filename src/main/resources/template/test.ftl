<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='style.css' rel='stylesheet' type='text/css'>
    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #aaa;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #aaa;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #aaa;
            color: #fff;
            background-color: #f38630;
        }

        .tg .tg-j2zy {
            background-color: #FCFBE3;
            vertical-align: top
        }

        .tg .tg-v163 {
            font-size: 16px;
            background-color: #9b9b9b;
            color: #ffffff;
            text-align: center;
            vertical-align: top
        }
    </style>
</head>
<body>

<table class="tg">
    <tr>
        <th class="tg-v163">Country ID</th>
        <th class="tg-v163">Country Name</th>
        <th class="tg-v163">User ID</th>
        <th class="tg-v163">User Name</th>
        <th class="tg-v163">Request ID</th>
        <th class="tg-v163">URL</th>
        <th class="tg-v163">Method</th>
        <th class="tg-v163">Params</th>
        <th class="tg-v163">Session ID</th>
        <th class="tg-v163">Session Date Opened</th>
        <th class="tg-v163">Session Date Closed</th>
    </tr>
    <#if requestDatas?has_content>
        <#list requestDatas as requestData>
            <#if requestData?has_content>
    <tr>
        <#if requestData.countryId?has_content>
            <td class="tg-yw4l">${requestData.countryId}</td></#if>
        <#if requestData.countryName?has_content>
            <td class="tg-yw4l">${requestData.countryName}</td></#if>
        <#if requestData.userId?has_content>
            <td class="tg-yw4l">${requestData.userId}</td></#if>
        <#if requestData.userName?has_content>
            <td class="tg-yw4l">${requestData.userName}</td></#if>
        <#if requestData.requestId?has_content>
            <td class="tg-yw4l">${requestData.requestId}</td></#if>
        <#if requestData.url?has_content>
            <td class="tg-yw4l">${requestData.url}</td></#if>
        <#if requestData.method?has_content>
            <td class="tg-yw4l">${requestData.method}</td></#if>
        <#if requestData.params?has_content>
            <td class="tg-yw4l">${requestData.params}</td></#if>
        <#if requestData.sessionId?has_content>
            <td class="tg-yw4l">${requestData.sessionId}</td></#if>
        <#if requestData.sessionDateOpened?has_content>
            <td class="tg-yw4l">${requestData.sessionDateOpened?string["EEE, dd MMMM yyyy"]}</td></#if>
        <#if requestData.sessionDateClosed?has_content>
            <td class="tg-yw4l">${requestData.sessionDateClosed?string["EEE, dd MMMM yyyy"]}</td></#if>
    </tr>
            </#if>
        </#list>
    </#if>
</table>
</body>
</html>
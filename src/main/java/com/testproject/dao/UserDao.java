package com.testproject.dao;

import com.testproject.entity.User;
import com.testproject.mappers.UserMapper;
import com.testproject.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class UserDao implements UserMapper {

    private static final Class<UserMapper> MAPPER_CLASS = UserMapper.class;

    @Override
    public void insertUser(User user) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).insertUser(user);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public User getUserById(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getUserById(id);
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void updateUser(User user) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).updateUser(user);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void deleteUser(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).deleteUser(id);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public List<User> getAllUsers() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllUsers();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }
}

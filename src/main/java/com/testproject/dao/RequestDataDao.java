package com.testproject.dao;

import com.testproject.entity.RequestData;
import com.testproject.mappers.RequestDataMapper;
import com.testproject.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class RequestDataDao implements RequestDataMapper {

    private static final Class<RequestDataMapper> MAPPER_CLASS = RequestDataMapper.class;

    @Override
    public List<RequestData> getAllRequestData() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllRequestData();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public List<RequestData> getAllRequestDataByDate(String filter) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllRequestDataByDate(filter);
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }
}

package com.testproject.dao;

import com.testproject.entity.Country;
import com.testproject.mappers.CountryMapper;
import com.testproject.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class CountryDao implements CountryMapper {

    private static final Class<CountryMapper> MAPPER_CLASS = CountryMapper.class;

    @Override
    public void insertCountry(Country country) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).insertCountry(country);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public Country getCountryById(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getCountryById(id);
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void updateCountry(Country country) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).updateCountry(country);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void deleteCountry(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).deleteCountry(id);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public List<Country> getAllCountries() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllCountries();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }
}

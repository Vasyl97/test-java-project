package com.testproject.dao;

import com.testproject.entity.Request;
import com.testproject.mappers.RequestMapper;
import com.testproject.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class RequestDao implements RequestMapper {

    private static final Class<RequestMapper> MAPPER_CLASS = RequestMapper.class;

    @Override
    public void insertRequest(Request request) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).insertRequest(request);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public Request getRequestById(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getRequestById(id);
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void updateRequest(Request request) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).updateRequest(request);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void deleteRequest(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).deleteRequest(id);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public List<Request> getAllRequests() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllRequests();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }
}

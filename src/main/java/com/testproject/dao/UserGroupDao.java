package com.testproject.dao;

import com.testproject.entity.UserGroup;
import com.testproject.mappers.UserGroupMapper;
import com.testproject.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class UserGroupDao implements UserGroupMapper {

    private static final Class<UserGroupMapper> MAPPER_CLASS = UserGroupMapper.class;

    @Override
    public void insertUserGroup(UserGroup userGroup) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).insertUserGroup(userGroup);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public UserGroup getUserGroupById(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getUserGroupById(id);
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void updateUserGroup(UserGroup userGroup) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).updateUserGroup(userGroup);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void deleteUserGroup(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).deleteUserGroup(id);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public List<UserGroup> getAllUserGroup() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllUserGroup();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }
}

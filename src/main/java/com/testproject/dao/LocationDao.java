package com.testproject.dao;

import com.testproject.entity.Location;
import com.testproject.mappers.LocationMapper;
import com.testproject.service.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class LocationDao implements LocationMapper {

    private static final Class<LocationMapper> MAPPER_CLASS = LocationMapper.class;

    @Override
    public void insertLocation(Location location) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).insertLocation(location);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public Location getLocationById(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getLocationById(id);
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void updateLocation(Location location) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).updateLocation(location);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public void deleteLocation(Integer id) {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            sqlSession.getMapper(MAPPER_CLASS).deleteLocation(id);
            sqlSession.commit();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }

    @Override
    public List<Location> getAllLocation() {
        SqlSession sqlSession = null;
        try {
            sqlSession = MyBatisUtil.openSession();
            return sqlSession.getMapper(MAPPER_CLASS).getAllLocation();
        } finally {
            if (sqlSession != null) {
                sqlSession.close();
            }
        }
    }
}

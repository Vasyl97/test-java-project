package com.testproject.service;

import com.testproject.dao.RequestDao;
import com.testproject.entity.Request;
import com.testproject.mappers.RequestMapper;

import java.util.List;

public class RequestService implements RequestMapper {

    private RequestDao requestDao = new RequestDao();

    @Override
    public void insertRequest(Request request) {
        requestDao.insertRequest(request);
    }

    @Override
    public Request getRequestById(Integer id) {
        return requestDao.getRequestById(id);
    }

    @Override
    public void updateRequest(Request request) {
        requestDao.updateRequest(request);
    }

    @Override
    public void deleteRequest(Integer id) {
        requestDao.deleteRequest(id);
    }

    @Override
    public List<Request> getAllRequests() {
        return requestDao.getAllRequests();
    }
}

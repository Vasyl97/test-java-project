package com.testproject.service;

import com.testproject.entity.RequestData;
import com.testproject.util.CONSTANT;
import com.testproject.util.EmailSender;
import com.testproject.util.FileBuilder;
import com.testproject.util.FreeMarker;

import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileReportService {
    private static final String FROM_EMAIL = "test20270@gmail.com";
    private static final String PASSWORD = "pass1234A";

    private RequestDataService requestDataService;
    private String email;
    private String format;
    private String filter;

    public FileReportService(String email, String format, String filter) {
        requestDataService = new RequestDataService();
        this.email = email;
        this.format = format;
        this.filter = filter;
    }

    public boolean send() {
        List<RequestData> requestData = (filter == null) ? requestDataService.getAllRequestData() : requestDataService.getAllRequestDataByDate(filter);

        StreamingOutput file;

        EmailSender emailSender = new EmailSender(FROM_EMAIL, PASSWORD);

        if (CONSTANT.XLS.toString().equalsIgnoreCase(format)) {
            file = outputStream -> writeXls(requestData, outputStream);
        } else if (CONSTANT.DOC.toString().equalsIgnoreCase(format)) {
            file = outputStream -> writeDoc(requestData, outputStream);
        } else if (CONSTANT.HTML.toString().equalsIgnoreCase(format)) {
            return sendHTML(requestData, emailSender);
        } else {
            return false;
        }

        try {
            emailSender.send(email, file, format);
        } catch (Exception ignore) {
            return false;
        }

        return true;
    }

    private boolean sendHTML(List<RequestData> requestData, EmailSender emailSender) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("requestDatas", requestData);
            emailSender.sendHtml(email, FreeMarker.getTemplate("test.ftl", map));
            return true;
        } catch (Exception e) {
           return false;
        }
    }

    private void writeXls(List<RequestData> requestData, OutputStream outputStream) throws IOException {
        try {
            FileBuilder.createXls(requestData, outputStream);
        } finally {
            outputStream.close();
        }
    }

    private void writeDoc(List<RequestData> requestData, OutputStream outputStream) throws IOException {
        try {
            FileBuilder.createDoc(requestData, outputStream);
        } finally {
            outputStream.close();
        }
    }
}

package com.testproject.service;

import com.testproject.util.FileBuilder;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

import java.io.InputStream;

public class MyBatisUtil {

    private static final Logger LOG = Logger.getLogger(FileBuilder.class);

    private static SqlSessionFactory sqlMapper = null;

    private MyBatisUtil() {
    }

    public static SqlSession openSession() {
        if (sqlMapper == null) {
            try {
                String resource = "mybatis-config.xml";
                InputStream inputStream = Resources.getResourceAsStream(resource);
                sqlMapper = new SqlSessionFactoryBuilder().build(inputStream);
                LOG.debug("openSession " + sqlMapper);
            } catch (Throwable x) {
                LOG.error(x.getMessage());
            }
        }

        return sqlMapper.openSession();
    }

}

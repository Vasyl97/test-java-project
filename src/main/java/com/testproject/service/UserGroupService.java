package com.testproject.service;

import com.testproject.dao.UserGroupDao;
import com.testproject.entity.UserGroup;
import com.testproject.mappers.UserGroupMapper;

import java.util.List;

public class UserGroupService implements UserGroupMapper {

    private UserGroupDao userGroupDao = new UserGroupDao();

    @Override
    public void insertUserGroup(UserGroup userGroup) {
        userGroupDao.insertUserGroup(userGroup);
    }

    @Override
    public UserGroup getUserGroupById(Integer id) {
        return userGroupDao.getUserGroupById(id);
    }

    @Override
    public void updateUserGroup(UserGroup userGroup) {
        userGroupDao.updateUserGroup(userGroup);
    }

    @Override
    public void deleteUserGroup(Integer id) {
        userGroupDao.deleteUserGroup(id);
    }

    @Override
    public List<UserGroup> getAllUserGroup() {
        return userGroupDao.getAllUserGroup();
    }
}

package com.testproject.service;

import com.testproject.dao.CountryDao;
import com.testproject.entity.Country;
import com.testproject.mappers.CountryMapper;

import java.util.List;

public class CountryService implements CountryMapper {

    private CountryDao countryDao = new CountryDao();

    @Override
    public void insertCountry(Country country) {
        countryDao.insertCountry(country);
    }

    @Override
    public Country getCountryById(Integer id) {
        return countryDao.getCountryById(id);
    }

    @Override
    public void updateCountry(Country country) {
        countryDao.updateCountry(country);
    }

    @Override
    public void deleteCountry(Integer id) {
        countryDao.deleteCountry(id);
    }

    @Override
    public List<Country> getAllCountries() {
        return countryDao.getAllCountries();
    }
}

package com.testproject.service;

import com.testproject.dao.UserDao;
import com.testproject.entity.User;
import com.testproject.mappers.UserMapper;

import java.util.List;

public class UserService implements UserMapper {

    private UserDao userDao = new UserDao();

    @Override
    public void insertUser(User user) {
        userDao.insertUser(user);
    }

    @Override
    public User getUserById(Integer id) {
        return userDao.getUserById(id);
    }

    @Override
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userDao.deleteUser(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }
}

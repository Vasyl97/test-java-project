package com.testproject.service;

import com.testproject.dao.RequestDataDao;
import com.testproject.entity.RequestData;
import com.testproject.mappers.RequestDataMapper;

import java.util.List;

public class RequestDataService implements RequestDataMapper {

    private RequestDataDao requestDataDao = new RequestDataDao();

    @Override
    public List<RequestData> getAllRequestData() {
        return requestDataDao.getAllRequestData();
    }

    @Override
    public List<RequestData> getAllRequestDataByDate(String filter) {
        return requestDataDao.getAllRequestDataByDate(filter);
    }
}

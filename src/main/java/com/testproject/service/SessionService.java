package com.testproject.service;

import com.testproject.dao.SessionDao;
import com.testproject.entity.Session;
import com.testproject.mappers.SessionMapper;

import java.util.List;

public class SessionService implements SessionMapper {

    private SessionDao sessionDao = new SessionDao();

    @Override
    public void insertSession(Session session) {
        sessionDao.insertSession(session);
    }

    @Override
    public Session getSessionById(Integer id) {
        return sessionDao.getSessionById(id);
    }

    @Override
    public void updateSession(Session session) {
        sessionDao.updateSession(session);
    }

    @Override
    public void deleteSession(Integer id) {
        sessionDao.deleteSession(id);
    }

    @Override
    public List<Session> getAllSessions() {
        return sessionDao.getAllSessions();
    }
}

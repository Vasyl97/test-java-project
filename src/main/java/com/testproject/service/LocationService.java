package com.testproject.service;

import com.testproject.dao.LocationDao;
import com.testproject.entity.Location;
import com.testproject.mappers.LocationMapper;

import java.util.List;

public class LocationService implements LocationMapper {

    private LocationDao locationDao = new LocationDao();

    @Override
    public void insertLocation(Location location) {
        locationDao.insertLocation(location);
    }

    @Override
    public Location getLocationById(Integer id) {
        return locationDao.getLocationById(id);
    }

    @Override
    public void updateLocation(Location location) {
        locationDao.updateLocation(location);
    }

    @Override
    public void deleteLocation(Integer id) {
        locationDao.deleteLocation(id);
    }

    @Override
    public List<Location> getAllLocation() {
        return locationDao.getAllLocation();
    }
}

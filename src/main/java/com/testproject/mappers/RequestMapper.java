package com.testproject.mappers;

import com.testproject.entity.Request;

import java.util.List;

public interface RequestMapper {

    void insertRequest(Request request);
    Request getRequestById(Integer id);
    void updateRequest(Request request);
    void deleteRequest(Integer id);
    List<Request> getAllRequests();
}

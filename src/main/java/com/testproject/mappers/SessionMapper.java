package com.testproject.mappers;

import com.testproject.entity.Session;

import java.util.List;

public interface SessionMapper {

    void insertSession(Session session);
    Session getSessionById(Integer id);
    void updateSession(Session session);
    void deleteSession(Integer id);
    List<Session> getAllSessions();
}

package com.testproject.mappers;

import com.testproject.entity.Location;

import java.util.List;

public interface LocationMapper {

    void insertLocation(Location location);
    Location getLocationById(Integer id);
    void updateLocation(Location location);
    void deleteLocation(Integer id);
    List<Location> getAllLocation();
}

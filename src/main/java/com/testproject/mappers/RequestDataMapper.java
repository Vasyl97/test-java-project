package com.testproject.mappers;

import com.testproject.entity.RequestData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RequestDataMapper {

    List<RequestData> getAllRequestData();

    List<RequestData> getAllRequestDataByDate(@Param("filter") String filter);

}

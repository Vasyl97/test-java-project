package com.testproject.mappers;

import com.testproject.entity.User;

import java.util.List;

public interface UserMapper {

    void insertUser(User user);
    User getUserById(Integer id);
    void updateUser(User user);
    void deleteUser(Integer id);
    List<User> getAllUsers();
}

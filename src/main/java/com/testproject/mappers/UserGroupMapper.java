package com.testproject.mappers;

import com.testproject.entity.UserGroup;

import java.util.List;

public interface UserGroupMapper {

    void insertUserGroup(UserGroup userGroup);
    UserGroup getUserGroupById(Integer id);
    void updateUserGroup(UserGroup userGroup);
    void deleteUserGroup(Integer id);
    List<UserGroup> getAllUserGroup();
}

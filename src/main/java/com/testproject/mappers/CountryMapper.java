package com.testproject.mappers;

import com.testproject.entity.Country;

import java.util.List;

public interface CountryMapper {

    void insertCountry(Country country);
    Country getCountryById(Integer id);
    void updateCountry(Country country);
    void deleteCountry(Integer id);
    List<Country> getAllCountries();
}

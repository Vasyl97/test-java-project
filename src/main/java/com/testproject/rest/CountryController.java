package com.testproject.rest;

import com.testproject.entity.Country;
import com.testproject.service.CountryService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/countries")
public class CountryController {
    CountryService countryService = new CountryService();

    @GET
    @Path("{id:\\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCountryById(@PathParam("id")Integer id){
       Country country= countryService.getCountryById(id);
        return Response.status(200).entity(country).build();
    }

    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCountries(){
        List<Country> countries = countryService.getAllCountries();
        return Response.status(200).entity(countries).build();
    }

    @PUT
    @Path("/put")
    @Consumes("application/json")
    public Response updateCountryInJSON(Country country) {
        countryService.updateCountry(country);
        String result = "Country update : " + country;
        return Response.status(200).entity(result).build();
    }

    @POST
    @Path("/post")
    @Consumes("application/json")
    public Response insertCountryInJSON(Country country) {
        countryService.insertCountry(country);
        String result = "Country saved : " + country;
        return Response.status(201).entity(result).build();
    }

    @DELETE
    @Path("{id:\\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCountry(@PathParam("id")Integer id) {
        countryService.deleteCountry(id);
        String result = "Country by id: " + id + " is deleted";
        return Response.status(200).entity(result).build();
    }

}

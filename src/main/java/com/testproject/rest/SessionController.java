package com.testproject.rest;

import com.testproject.entity.Session;
import com.testproject.service.SessionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/sessions")
public class SessionController {
    SessionService sessionService = new SessionService();

        @GET
        @Path("{id:\\d+}")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getSessionById(@PathParam("id")Integer id){
            Session session = sessionService.getSessionById(id);
            return Response.status(200).entity(session).build();
        }

        @GET
        @Path("/getAll")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getAllSessions(){
            List<Session> sessions = sessionService.getAllSessions();
            return Response.status(200).entity(sessions).build();
        }

        @PUT
        @Path("/put")
        @Consumes("application/json")
        public Response updateSessionInJSON(Session session) {
            sessionService.updateSession(session);
            String result = "Session update : " + session;
            return Response.status(200).entity(result).build();
        }

        @POST
        @Path("/post")
        @Consumes("application/json")
        public Response insertSessionInJSON(Session session) {
            sessionService.insertSession(session);
            String result = "Session saved : " + session;
            return Response.status(201).entity(result).build();
        }

        @DELETE
        @Path("{id:\\d+}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response deleteSession(@PathParam("id")Integer id) {
            sessionService.deleteSession(id);
            String result = "Session by id: " + id + " is deleted";
            return Response.status(200).entity(result).build();
        }

}

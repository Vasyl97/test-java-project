package com.testproject.rest;

import com.testproject.entity.Request;
import com.testproject.service.RequestService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/requests")
public class RequestController {
    RequestService requestService = new RequestService();

        @GET
        @Path("{id:\\d+}")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getRequestById(@PathParam("id")Integer id){
            Request request= requestService.getRequestById(id);
            return Response.status(200).entity(request).build();
        }

        @GET
        @Path("/getAll")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getAllRequests(){
            List<Request> requests = requestService.getAllRequests();
            return Response.status(200).entity(requests).build();
        }

        @PUT
        @Path("/put")
        @Consumes("application/json")
        public Response updateRequestInJSON(Request request) {
            requestService.updateRequest(request);
            String result = "Request update : " + request;
            return Response.status(200).entity(result).build();
        }

        @POST
        @Path("/post")
        @Consumes("application/json")
        public Response insertRequestInJSON(Request request) {
            requestService.insertRequest(request);
            String result = "Request saved : " + request;
            return Response.status(201).entity(result).build();
        }

        @DELETE
        @Path("{id:\\d+}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response deleteRequest(@PathParam("id")Integer id) {
            requestService.deleteRequest(id);
            String result = "Request by id: " + id + " is deleted";
            return Response.status(200).entity(result).build();
        }

    }

package com.testproject.rest;

import com.testproject.service.FileReportService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("/report")
public class Report {

    @GET
    @Path("/get_document")
    public Response getFile(@QueryParam("email") String email, @QueryParam("format") String format, @QueryParam("filter") String filter) {
        FileReportService service = new FileReportService(email, format, filter);
        if (service.send()) {
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
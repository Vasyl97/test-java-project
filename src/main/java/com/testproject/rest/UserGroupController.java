package com.testproject.rest;

import com.testproject.entity.UserGroup;
import com.testproject.service.UserGroupService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/user_groups")
public class UserGroupController {
    UserGroupService userGroupService = new UserGroupService();

        @GET
        @Path("{id:\\d+}")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getUserGroupById(@PathParam("id")Integer id){
            UserGroup userGroup= userGroupService.getUserGroupById(id);
            return Response.status(200).entity(userGroup).build();
        }

        @GET
        @Path("/getAll")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getAllUserGroups(){
            List<UserGroup> userGroups = userGroupService.getAllUserGroup();
            return Response.status(200).entity(userGroups).build();
        }

        @PUT
        @Path("/put")
        @Consumes("application/json")
        public Response updateUserGroupInJSON(UserGroup userGroup) {
            userGroupService.updateUserGroup(userGroup);
            String result = "UserGroup update : " + userGroup;
            return Response.status(200).entity(result).build();
        }

        @POST
        @Path("/post")
        @Consumes("application/json")
        public Response insertUserGroupInJSON(UserGroup userGroup) {
            userGroupService.insertUserGroup(userGroup);
            String result = "UserGroup saved : " + userGroup;
            return Response.status(201).entity(result).build();
        }

        @DELETE
        @Path("{id:\\d+}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response deleteUserGroup(@PathParam("id")Integer id) {
            userGroupService.deleteUserGroup(id);
            String result = "UserGroup by id: " + id + " is deleted";
            return Response.status(200).entity(result).build();
        }

    }

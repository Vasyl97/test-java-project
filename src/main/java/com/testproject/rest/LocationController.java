package com.testproject.rest;

import com.testproject.entity.Location;
import com.testproject.service.LocationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/locations")
public class LocationController {
    LocationService locationService = new LocationService();

        @GET
        @Path("{id:\\d+}")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getLocationById(@PathParam("id")Integer id){
            Location location= locationService.getLocationById(id);
            return Response.status(200).entity(location).build();
        }

        @GET
        @Path("/getAll")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getAllLocations(){
            List<Location> locations = locationService.getAllLocation();
            return Response.status(200).entity(locations).build();
        }

        @PUT
        @Path("/put")
        @Consumes("application/json")
        public Response updateLocationInJSON(Location location) {
            locationService.updateLocation(location);
            String result = "Location update : " + location;
            return Response.status(200).entity(result).build();
        }

        @POST
        @Path("/post")
        @Consumes("application/json")
        public Response insertLocationInJSON(Location location) {
            locationService.insertLocation(location);
            String result = "Location saved : " + location;
            return Response.status(201).entity(result).build();
        }

        @DELETE
        @Path("{id:\\d+}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response deleteLocation(@PathParam("id")Integer id) {
            locationService.deleteLocation(id);
            String result = "Location by id: " + id + " is deleted";
            return Response.status(200).entity(result).build();
        }


}

package com.testproject.rest;

import com.testproject.entity.User;
import com.testproject.service.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
public class UserController {
    UserService userService = new UserService();

        @GET
        @Path("{id:\\d+}")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getUserById(@PathParam("id")Integer id){
            User user= userService.getUserById(id);
            return Response.status(200).entity(user).build();
        }

        @GET
        @Path("/getAll")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getAllUsers(){
            List<User> users = userService.getAllUsers();
            return Response.status(200).entity(users).build();
        }

        @PUT
        @Path("/put")
        @Consumes("application/json")
        public Response updateUserInJSON(User user) {
            userService.updateUser(user);
            String result = "User update : " + user;
            return Response.status(200).entity(result).build();
        }

        @POST
        @Path("/post")
        @Consumes("application/json")
        public Response insertUserInJSON(User user) {
            userService.insertUser(user);
            String result = "User saved : " + user;
            return Response.status(201).entity(result).build();
        }

        @DELETE
        @Path("{id:\\d+}")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response deleteUser(@PathParam("id")Integer id) {
            userService.deleteUser(id);
            String result = "User by id: " + id + " is deleted";
            return Response.status(200).entity(result).build();
        }


    }

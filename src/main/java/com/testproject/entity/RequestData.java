package com.testproject.entity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Date;

public class RequestData {

    @Retention(RetentionPolicy.RUNTIME)
    public @interface Order {
        int value();
        String name();
    }

    private Integer countryId;
    private String countryName;
    private Integer userId;
    private String userName;
    private Integer requestId;
    private String url;
    private String method;
    private String params;
    private Integer sessionId;
    private Date sessionDateOpened;
    private Date sessionDateClosed;

    @Order(value = 0, name = "Country ID")
    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    @Order(value = 1, name = "Country name")
    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Order(value = 2, name = "User ID")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Order(value = 3, name = "User name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Order(value = 4, name = "Request ID")
    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    @Order(value = 5, name = "Url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Order(value = 6, name = "Method")
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Order(value = 7, name = "Params")
    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Order(value = 8, name = "Session ID")
    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    @Order(value = 9, name = "Session date opened")
    public Date getSessionDateOpened() {
        return sessionDateOpened;
    }

    public void setSessionDateOpened(Date sessionDateOpened) {
        this.sessionDateOpened = sessionDateOpened;
    }

    @Order(value = 10, name = "Session date closed")
    public Date getSessionDateClosed() {
        return sessionDateClosed;
    }

    public void setSessionDateClosed(Date sessionDateClosed) {
        this.sessionDateClosed = sessionDateClosed;
    }

    @Override
    public String toString() {
        return "RequestData{" +
                "countryId=" + countryId +
                ", countryName='" + countryName + '\'' +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", requestId=" + requestId +
                ", url='" + url + '\'' +
                ", method='" + method + '\'' +
                ", params='" + params + '\'' +
                ", sessionId=" + sessionId +
                ", sessionDateOpened=" + sessionDateOpened +
                ", sessionDateClosed=" + sessionDateClosed +
                '}';
    }
}

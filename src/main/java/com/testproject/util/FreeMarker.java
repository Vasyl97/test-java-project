package com.testproject.util;

import com.testproject.entity.RequestData;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

import java.io.IOException;
import java.io.StringWriter;

public class FreeMarker {
    private static Configuration configuration;

    private static Configuration  setConfiguration(){
        if (configuration == null){
            configuration = new Configuration(new Version("2.3.23"));
            configuration.setClassForTemplateLoading(FreeMarker.class, "/template");
            configuration.setDefaultEncoding("UTF-8");
        }
        return configuration;
    }

    public static String getTemplate(String templateName, Object object) throws IOException, TemplateException{
        Template template = setConfiguration().getTemplate(templateName);
        try (StringWriter out = new StringWriter()) {
            template.process(object, out);
            out.flush();
            return out.toString();
        }
    }
}

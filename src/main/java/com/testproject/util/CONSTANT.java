package com.testproject.util;

public enum  CONSTANT {XLS, DOC, HTML;

    @Override
    public String toString() {
        return name().toUpperCase(java.util.Locale.US);
    }
}
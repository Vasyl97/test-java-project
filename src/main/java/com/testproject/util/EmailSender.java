package com.testproject.util;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.core.StreamingOutput;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class EmailSender {

    private Session session;
    private String fromEmail;

    public EmailSender(String fromEmail, String password) {
        this.fromEmail = fromEmail;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        this.session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        });
    }

    public void send(String toEmail, StreamingOutput so, String format) throws MessagingException, IOException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(fromEmail));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
        message.setSubject("Report file");
        message.setSentDate(new Date());
        message.setText("Report");

        Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        so.write(output);

        DataSource aAttachment = new ByteArrayDataSource(output.toByteArray(), "application/octet-stream");
        messageBodyPart.setDataHandler(new DataHandler(aAttachment));
        messageBodyPart.setHeader("Content-Type", "text/plain; charset=\"us-ascii\"; name=\"report." + format + "\"");
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);
        Transport.send(message);
    }

    public void sendHtml(String toEmail, String report) throws MessagingException{
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(fromEmail));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
        message.setSubject("Report file");
        message.setSentDate(new Date());
        message.setText(report);
        message.setContent(report, "text/html; charset=utf-8");

        Transport.send(message);
    }
}

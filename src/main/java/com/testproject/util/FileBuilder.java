package com.testproject.util;

import com.testproject.entity.RequestData;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class FileBuilder {

    private static final Logger log = Logger.getLogger(FileBuilder.class);

    public static void createXls(List<RequestData> data, OutputStream os) {
        HSSFWorkbook document = new HSSFWorkbook();
        HSSFSheet sheet = document.createSheet("name");
        HSSFRow row = sheet.createRow(0);

        getRequestDataMethods().forEach(m -> initXls(m, row));

        IntStream.range(0, 10).forEach(sheet::autoSizeColumn);
        IntStream.range(0, data.size()).forEach(i -> writeXls(sheet, data.get(i), i + 1));

        try {
            document.write(os);
        } catch (IOException e) {
            log.error(e);
        }
    }

    public static void createDoc(List<RequestData> data, OutputStream os) {
        XWPFDocument document = new XWPFDocument();
        XWPFTable table = document.createTable();

        XWPFTableRow tableRowOne = table.getRow(0);

        getRequestDataMethods().forEach(m -> initDoc(m, tableRowOne));

        data.forEach(r -> writeDoc(table, r));

        try {
            document.write(os);
        } catch (IOException e) {
            log.error(e);
        }
    }

    private static void initXls(Method method, HSSFRow row) {
        if (filterMethod(method)) {
            row.createCell(method.getAnnotation(RequestData.Order.class).value()).setCellValue(method.getAnnotation(RequestData.Order.class).name());
        }
    }

    private static void initDoc(Method method, XWPFTableRow tableRowOne) {
        if (filterMethod(method)) {
            tableRowOne.getCell(method.getAnnotation(RequestData.Order.class).value()).setText(method.getAnnotation(RequestData.Order.class).name());
            tableRowOne.addNewTableCell();
        }
    }


    private static void writeXls(HSSFSheet sheet, RequestData requestData, int rowIndex) {
        HSSFRow row = sheet.createRow(rowIndex);
        getRequestDataMethods().forEach(method -> fillRowXls(method, row, requestData));
    }

    private static void fillRowXls(Method method, HSSFRow row, RequestData requestData) {
        if (filterMethod(method)) {
            try {
                row.createCell(method.getAnnotation(RequestData.Order.class).value()).setCellValue(String.valueOf(method.invoke(requestData)));
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error(e);
            }
        }
    }

    private static void writeDoc(XWPFTable table, RequestData requestData) {
        XWPFTableRow tableRowTwo = table.createRow();
        getRequestDataMethods().forEach(method -> fillRowDoc(method, tableRowTwo, requestData));
    }

    private static void fillRowDoc(Method method, XWPFTableRow tableRowTwo, RequestData requestData) {
        if (filterMethod(method)) {
            try {
                tableRowTwo.getCell(method.getAnnotation(RequestData.Order.class).value()).setText(String.valueOf(method.invoke(requestData)));
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error(e);
            }
        }
    }

    private static List<Method> getRequestDataMethods() {
        List<Method> methods = Arrays.asList(RequestData.class.getDeclaredMethods());
        methods.sort((o1, o2) -> sortRequestDataOrder(o1, o2));

        return methods;
    }

    private static int sortRequestDataOrder(Method o1, Method o2) {
        RequestData.Order or1 = o1.getAnnotation(RequestData.Order.class);
        RequestData.Order or2 = o2.getAnnotation(RequestData.Order.class);
        if (or1 != null && or2 != null) {
            return or1.value() - or2.value();
        } else if (or1 != null && or2 == null) {
            return -1;
        } else if (or1 == null && or2 != null) {
            return 1;
        }
        return o1.getName().compareTo(o2.getName());
    }

    private static boolean filterMethod(Method method) {
        return (Modifier.isPublic(method.getModifiers())
                && method.getParameterCount() == 0
                && method.getName().startsWith("get")
                && !void.class.equals(method.getReturnType()));

    }

}


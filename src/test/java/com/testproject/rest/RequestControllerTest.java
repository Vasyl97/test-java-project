package com.testproject.rest;

import com.testproject.entity.Location;
import com.testproject.entity.Request;
import com.testproject.entity.UserGroup;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.List;

import static org.junit.Assert.*;

public class RequestControllerTest {
    private RequestController requestController;
    private Response response;
    private Request result;
    private static Integer id;

    @Before
    public void setUp() throws Exception {
        requestController = new RequestController();
    }

    @After
    public void tearDown() throws Exception {
        requestController = null;
    }

    @Test
    public void getRequestById() {
        response = requestController.getRequestById(1);
        result = (Request) response.getEntity();
        Assert.assertNotNull(result);
        Assert.assertTrue(response.getStatus()==200);
    }

    @Test
    public void getAllRequests() {
        response = requestController.getAllRequests();
        List<Request> result = (List<Request>) response.getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNotNull(result);
    }

    @Test
    public void updateRequestInJSON() {
        result = (Request) requestController.getRequestById(1).getEntity();
        result.setMethod("Test");
        response = requestController.updateRequestInJSON(result);
        Assert.assertTrue(response.getStatus()==200);
        Request result2 = (Request) requestController.getRequestById(1).getEntity();
        Assert.assertEquals(result.toString(), result2.toString());
    }

    @Test
    public void insertRequestInJSON() {
        Request test = new Request();
        test.setMethod("Test");
        test.setParams("Test");
        response = requestController.insertRequestInJSON(test);
        Assert.assertTrue(response.getStatus()==201);
        id = test.getId();
        result = (Request) requestController.getRequestById(id).getEntity();
        Assert.assertEquals(test.toString(), result.toString());
    }

    @Test
    public void deleteRequest() {
        response = requestController.deleteRequest(id);
        result = (Request) requestController.getRequestById(id).getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNull(result);
    }
}
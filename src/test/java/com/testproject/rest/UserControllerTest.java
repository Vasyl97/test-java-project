package com.testproject.rest;

import com.testproject.entity.Session;
import com.testproject.entity.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class UserControllerTest {
    private UserController userController;
    private Response response;
    private User result;
    private static Integer id;

    @Before
    public void setUp() throws Exception {
        userController = new UserController();
    }

    @After
    public void tearDown() throws Exception {
        userController = null;
    }

    @Test
    public void getUserById() {
        response = userController.getUserById(1);
        result = (User) response.getEntity();
        Assert.assertNotNull(result);
        Assert.assertTrue(response.getStatus()==200);
    }

    @Test
    public void getAllUsers() {
        response = userController.getAllUsers();
        List<User> result = (List<User>) response.getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNotNull(result);
    }

    @Test
    public void updateUserInJSON() {
        result = (User) userController.getUserById(1).getEntity();
        result.setUserName("Test");
        response = userController.updateUserInJSON(result);
        Assert.assertTrue(response.getStatus()==200);
        User result2 = (User) userController.getUserById(1).getEntity();
        Assert.assertEquals(result.toString(), result2.toString());
    }

    @Test
    public void insertUserInJSON() {
        User test = new User();
        test.setUserName("Test");
        response = userController.insertUserInJSON(test);
        Assert.assertTrue(response.getStatus()==201);
        id = test.getId();
        result = (User) userController.getUserById(id).getEntity();
        Assert.assertEquals(test.toString(), result.toString());
    }

    @Test
    public void deleteUser() {
        response = userController.deleteUser(id);
        result = (User) userController.getUserById(id).getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNull(result);
    }
}
package com.testproject.rest;

import com.testproject.entity.Country;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.List;

public class CountryControllerTest {
    private CountryController countryController;
    private Response response;
    private Country result;
    private static Integer id;

    @Before
    public void setUp() throws Exception {
        countryController = new CountryController();
    }

    @After
    public void tearDown() throws Exception {
        countryController = null;
    }

    @Test
    public void getCountryById() {
        response = countryController.getCountryById(1);
        result = (Country) response.getEntity();
        Assert.assertNotNull(result);
        Assert.assertTrue(response.getStatus()==200);
    }

    @Test
    public void getAllCountries() {
        response = countryController.getAllCountries();
        List<Country> result = (List<Country>) response.getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNotNull(result);
    }

    @Test
    public void updateCountryInJSON() {
        result = (Country) countryController.getCountryById(1).getEntity();
        result.setLanguage("test");
        response = countryController.updateCountryInJSON(result);
        Assert.assertTrue(response.getStatus()==200);
        Country result2 = (Country) countryController.getCountryById(1).getEntity();
        Assert.assertEquals(result.toString(), result2.toString());
    }

    @Test
    public void insertCountryInJSON() {
        Country test = new Country();
        test.setLanguage("Test");
        test.setCountryName("Test");
        response = countryController.insertCountryInJSON(test);
        Assert.assertTrue(response.getStatus()==201);
        id = test.getId();
        result = (Country) countryController.getCountryById(id).getEntity();
        Assert.assertEquals(test.toString(), result.toString());
    }

    @Test
    public void deleteCountry() {
        response = countryController.deleteCountry(id);
        result = (Country) countryController.getCountryById(id).getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNull(result);
    }
}
package com.testproject.rest;

import com.testproject.entity.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.Date;
import java.util.List;

public class SessionControllerTest {
    private SessionController sessionController;
    private Response response;
    private Session result;
    private static Integer id;

    @Before
    public void setUp() throws Exception {
        sessionController = new SessionController();
    }

    @After
    public void tearDown() throws Exception {
        sessionController = null;
    }

    @Test
    public void getSessionById() {
        response = sessionController.getSessionById(1);
        result = (Session) response.getEntity();
        Assert.assertNotNull(result);
        Assert.assertTrue(response.getStatus()==200);
    }

    @Test
    public void getAllSessions() {
        response = sessionController.getAllSessions();
        List<Session> result = (List<Session>) response.getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNotNull(result);
    }

    @Test
    public void updateSessionInJSON() {
        result = (Session) sessionController.getSessionById(1).getEntity();
        result.setDateClosed(new Date());
        response = sessionController.updateSessionInJSON(result);
        Assert.assertTrue(response.getStatus()==200);
        Session result2 = (Session) sessionController.getSessionById(1).getEntity();
        Assert.assertEquals(result.toString(), result2.toString());
    }

    @Test
    public void insertSessionInJSON() {
        Session test = new Session();
        test.setDateClosed(new Date());
        response = sessionController.insertSessionInJSON(test);
        Assert.assertTrue(response.getStatus()==201);
        id = test.getId();
        result = (Session) sessionController.getSessionById(id).getEntity();
        Assert.assertEquals(test.toString(), result.toString());
    }

    @Test
    public void deleteSession() {
        response = sessionController.deleteSession(id);
        result = (Session) sessionController.getSessionById(id).getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNull(result);
    }
}
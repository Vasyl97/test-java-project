package com.testproject.rest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class ReportTest {
    private Report report;

    @Before
    public void setUp() throws Exception {
        report = new Report();
    }

    @After
    public void tearDown() throws Exception {
        report = null;
    }

    @Test
    public void getFile() {
        Assert.assertTrue(report.getFile("wsevolodmail@gmail.com","doc", null).getStatus()==200);
    }
}
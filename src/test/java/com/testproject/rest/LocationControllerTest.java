package com.testproject.rest;

import com.testproject.entity.Country;
import com.testproject.entity.Location;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.List;

public class LocationControllerTest {
    private LocationController locationController;
    private Response response;
    private Location result;
    private static Integer id;

    @Before
    public void setUp() throws Exception {
        locationController = new LocationController();
    }

    @After
    public void tearDown() throws Exception {
        locationController = null;
    }

    @Test
    public void getLocationById() {
        response = locationController.getLocationById(1);
        result = (Location) response.getEntity();
        Assert.assertNotNull(result);
        Assert.assertTrue(response.getStatus()==200);
    }

    @Test
    public void getAllLocations() {
        response = locationController.getAllLocations();
        List<Location> result = (List<Location>) response.getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNotNull(result);
    }

    @Test
    public void updateLocationInJSON() {
        result = (Location) locationController.getLocationById(1).getEntity();
        result.setLocationName("Test");
        response = locationController.updateLocationInJSON(result);
        Assert.assertTrue(response.getStatus()==200);
        Location result2 = (Location) locationController.getLocationById(1).getEntity();
        Assert.assertEquals(result.toString(), result2.toString());
    }

    @Test
    public void insertLocationInJSON() {
        Location test = new Location();
        test.setLocationName("test");
        test.setLatitude(200.300);
        test.setLongitude(300.200);
        response = locationController.insertLocationInJSON(test);
        Assert.assertTrue(response.getStatus()==201);
        id = test.getId();
        result = (Location) locationController.getLocationById(id).getEntity();
        Assert.assertEquals(test.toString(), result.toString());
    }

    @Test
    public void deleteLocation() {
        response = locationController.deleteLocation(id);
        result = (Location) locationController.getLocationById(id).getEntity();
        Assert.assertTrue(response.getStatus()==200);
        Assert.assertNull(result);
    }
}
package com.testproject.rest;

import com.testproject.entity.UserGroup;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.List;


public class UserGroupControllerTest {
    private UserGroupController userGroupController;
    private Response response;
    private UserGroup result;
    private static Integer id;

    @Before
    public void setUp() throws Exception {
        userGroupController = new UserGroupController();
    }

    @After
    public void tearDown() throws Exception {
        userGroupController = null;
    }

    @Test
    public void getUserGroupById() {
        response = userGroupController.getUserGroupById(1);
        result = (UserGroup) response.getEntity();
        Assert.assertNotNull(result);
        Assert.assertTrue(response.getStatus() == 200);
    }

        @Test
    public void getAllUserGroups() {
            response = userGroupController.getAllUserGroups();
            List<UserGroup> result = (List<UserGroup>) response.getEntity();
            Assert.assertNotNull(result);
            Assert.assertTrue(response.getStatus()==200);
    }

    @Test
    public void updateUserGroupInJSON() {
        result = (UserGroup) userGroupController.getUserGroupById(1).getEntity();
        result.setGroupName("Test");
        response = userGroupController.updateUserGroupInJSON(result);
        Assert.assertTrue(response.getStatus()==200);
        UserGroup result2 = (UserGroup) userGroupController.getUserGroupById(1).getEntity();
        Assert.assertEquals(result.toString(),result2.toString());
    }

    @Test
    public void insertUserGroupInJSON() {
        UserGroup test = new UserGroup();
        test.setGroupName("The Best");
        response = userGroupController.insertUserGroupInJSON(test);
        Assert.assertTrue(response.getStatus()==201);
        id = test.getId();
        result = (UserGroup) userGroupController.getUserGroupById(id).getEntity();
        Assert.assertEquals(test.toString(),result.toString());
    }

    @Test
    public void deleteUserGroup() {
        response = userGroupController.deleteUserGroup(id);
        Assert.assertTrue(response.getStatus()==200);
        result =(UserGroup) userGroupController.getUserGroupById(id).getEntity();
        Assert.assertNull(result);
    }
}
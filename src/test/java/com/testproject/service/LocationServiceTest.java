package com.testproject.service;

import com.testproject.entity.Location;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class LocationServiceTest {

    private LocationService locationService;

    @Before
    public void setUp() throws Exception {
        locationService = new LocationService();
    }

    @After
    public void tearDown() throws Exception {
        locationService = null;
    }

    @Test
    public void insertLocation() {
        Location location = new Location();
        location.setLocationName("Location name");
        location.setCountryId(1);
        location.setLatitude(22.33);
        location.setLongitude(33.33);

        locationService.insertLocation(location);
        Assert.assertTrue(location.getId() != 0);

        Location locationInsert = locationService.getLocationById(location.getId());
        System.out.println();
        Assert.assertNotNull(locationInsert);
        Assert.assertEquals(locationInsert.getLocationName(), location.getLocationName());
        Assert.assertEquals(locationInsert.getCountryId(), location.getCountryId());
        Assert.assertEquals(locationInsert.getLatitude(), location.getLatitude(), 0.001);
        Assert.assertEquals(locationInsert.getLongitude(), location.getLongitude(), 0.001);
    }

    @Test
    public void getLocationById() {
        Location location = locationService.getLocationById(1);
        Assert.assertNotNull(location);
    }

    @Test
    public void updateLocation() {
        Location location = locationService.getLocationById(4);
        location.setLocationName("Location name" + System.currentTimeMillis());
        location.setCountryId(2);
        location.setLatitude(44.22);
        location.setLongitude(44.22);

        locationService.updateLocation(location);

        Location locationUpdate = locationService.getLocationById(4);
        Assert.assertEquals(locationUpdate.getLocationName(), location.getLocationName());
        Assert.assertEquals(locationUpdate.getCountryId(), location.getCountryId());
        Assert.assertEquals(locationUpdate.getLatitude(), location.getLatitude(), 0.001);
        Assert.assertEquals(locationUpdate.getLongitude(), location.getLongitude(), 0.001);
    }

    @Test
    public void deleteLocation() {
        Location locationInsert = new Location();
        locationInsert.setLocationName("Location name");
        locationInsert.setCountryId(1);
        locationInsert.setLatitude(22.33);
        locationInsert.setLongitude(33.33);
        locationService.insertLocation(locationInsert);

        Location location = locationService.getLocationById(locationInsert.getId());
        locationService.deleteLocation(location.getId());
        Location locationDeleted = locationService.getLocationById(locationInsert.getId());
        Assert.assertNull(locationDeleted);

    }

    @Test
    public void getAllLocation() {
        List<Location> locations = locationService.getAllLocation();
        Assert.assertNotNull(locations);
    }
}
package com.testproject.service;

import com.testproject.entity.Session;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class SessionServiceTest {

    private SessionService sessionService;

    @Before
    public void setUp() throws Exception {
        sessionService = new SessionService();
    }

    @After
    public void tearDown() throws Exception {
        sessionService = null;
    }

    @Test
    public void insertSession() {
        Date date = new Date();

        Session session = new Session();
        session.setUserId(1);
        session.setDateOpened(date);
        session.setDateClosed(date);

        sessionService.insertSession(session);
        Assert.assertTrue(session.getId() != 0);
        Session sessionInsert = sessionService.getSessionById(session.getId());
        Assert.assertNotNull(sessionInsert);
        Assert.assertEquals(sessionInsert.getUserId(), session.getUserId());
        Assert.assertEquals(sessionInsert.getDateOpened().toString(), session.getDateOpened().toString());
        Assert.assertEquals(sessionInsert.getDateClosed().toString(), session.getDateClosed().toString());
    }

    @Test
    public void getSessionById() {
        Session session = sessionService.getSessionById(1);
        Assert.assertNotNull(session);
    }

    @Test
    public void updateSession() {
        Session session = sessionService.getSessionById(4);
        session.setUserId(2);
        session.setDateOpened(new Date());
        session.setDateClosed(new Date());

        sessionService.updateSession(session);

        Session sessionUpdate = sessionService.getSessionById(4);
        Assert.assertEquals(sessionUpdate.getUserId(), session.getUserId());
        Assert.assertEquals(sessionUpdate.getDateOpened().toString(), session.getDateOpened().toString());
        Assert.assertEquals(sessionUpdate.getDateClosed().toString(), session.getDateClosed().toString());
    }

    @Test
    public void deleteSession() {
        Session sessionInsert = new Session();
        sessionInsert.setUserId(1);
        sessionInsert.setDateOpened(new Date());
        sessionInsert.setDateClosed(new Date());
        sessionService.insertSession(sessionInsert);

        Session session = sessionService.getSessionById(sessionInsert.getId());
        sessionService.deleteSession(session.getId());
        Session sessionDeleted = sessionService.getSessionById(sessionInsert.getId());
        Assert.assertNull(sessionDeleted);
    }

    @Test
    public void getAllSessions() {
        List<Session> sessions = sessionService.getAllSessions();
        Assert.assertNotNull(sessions);
    }
}
package com.testproject.service;

import com.testproject.entity.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class UserServiceTest {

    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userService = new UserService();
    }

    @After
    public void tearDown() throws Exception {
        userService = null;
    }

    @Test
    public void insertUser() {
        User user = new User();
        user.setUserName("User name");
        user.setLocationId(1);
        user.setUserGroupId(1);

        userService.insertUser(user);
        Assert.assertTrue(user.getId() != 0);

        User userInsert = userService.getUserById(user.getId());
        Assert.assertNotNull(userInsert);
        Assert.assertEquals(userInsert.getUserName(), user.getUserName());
        Assert.assertEquals(userInsert.getLocationId(), user.getLocationId());
        Assert.assertEquals(userInsert.getUserGroupId(), user.getUserGroupId());
    }

    @Test
    public void getUserById() {
        User user = userService.getUserById(1);
        Assert.assertNotNull(user);
    }

    @Test
    public void updateUser() {
        User user = userService.getUserById(6);
        user.setUserName("User name" + System.currentTimeMillis());
        user.setLocationId(2);
        user.setUserGroupId(2);

        userService.updateUser(user);

        User userUpdate = userService.getUserById(6);
        Assert.assertEquals(userUpdate.getUserName(), user.getUserName());
        Assert.assertEquals(userUpdate.getLocationId(), user.getLocationId());
        Assert.assertEquals(userUpdate.getUserGroupId(), user.getUserGroupId());
    }

    @Test
    public void deleteUser() {
        User userInsert = new User();
        userInsert.setUserName("User name");
        userInsert.setLocationId(1);
        userInsert.setUserGroupId(1);
        userService.insertUser(userInsert);

        User user = userService.getUserById(userInsert.getId());
        userService.deleteUser(user.getId());
        User userDeleted = userService.getUserById(userInsert.getId());
        Assert.assertNull(userDeleted);
    }

    @Test
    public void getAllUsers() {
        List<User> users = userService.getAllUsers();
        Assert.assertNotNull(users);
    }
}
package com.testproject.service;

import com.testproject.entity.Request;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class RequestServiceTest {

    private RequestService requestService;

    @Before
    public void setUp() throws Exception {
        requestService = new RequestService();
    }

    @After
    public void tearDown() throws Exception {
        requestService = null;
    }

    @Test
    public void insertRequest() {
        Request request = new Request();
        request.setUrl("request url");
        request.setMethod("request m");
        request.setParams("request params");
        request.setSessionId(1);

        requestService.insertRequest(request);
        Assert.assertTrue(request.getId() != 0);

        Request requestInsert = requestService.getRequestById(request.getId());
        Assert.assertNotNull(requestInsert);
        Assert.assertEquals(requestInsert.getUrl(), request.getUrl());
        Assert.assertEquals(requestInsert.getMethod(), request.getMethod());
        Assert.assertEquals(requestInsert.getParams(), request.getParams());
        Assert.assertEquals(requestInsert.getSessionId(), request.getSessionId());
    }

    @Test
    public void getRequestById() {
        Request request = requestService.getRequestById(8);
        Assert.assertNotNull(request);
    }

    @Test
    public void updateRequest() {
        Request request = requestService.getRequestById(12);
        request.setUrl("url");
        request.setMethod("method");
        request.setParams("params");
        request.setSessionId(2);

        requestService.updateRequest(request);

        Request requestUpdate = requestService.getRequestById(12);
        Assert.assertEquals(requestUpdate.getUrl(), request.getUrl());
        Assert.assertEquals(requestUpdate.getMethod(), request.getMethod());
        Assert.assertEquals(requestUpdate.getParams(), request.getParams());
        Assert.assertEquals(requestUpdate.getSessionId(), request.getSessionId());
    }

    @Test
    public void deleteRequest() {
        Request requestInsert = new Request();
        requestInsert.setUrl("request url");
        requestInsert.setMethod("request m");
        requestInsert.setParams("request params");
        requestInsert.setSessionId(1);
        requestService.insertRequest(requestInsert);

        Request request = requestService.getRequestById(requestInsert.getId());
        requestService.deleteRequest(request.getId());
        Request requestDeleted = requestService.getRequestById(requestInsert.getId());
        Assert.assertNull(requestDeleted);
    }

    @Test
    public void getAllRequests() {
        List<Request> requests = requestService.getAllRequests();
        Assert.assertNotNull(requests);
    }
}
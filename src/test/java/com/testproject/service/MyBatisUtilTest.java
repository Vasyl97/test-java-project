package com.testproject.service;


import org.apache.ibatis.session.SqlSession;
import org.junit.Assert;
import org.junit.Test;

public class MyBatisUtilTest {

    @Test
    public void getSqlSessionFactory() {
        SqlSession sqlSession = MyBatisUtil.openSession();
        Assert.assertNotNull(sqlSession);
        if (sqlSession != null) {
            sqlSession.close();
        }
    }
}
package com.testproject.service;

import com.testproject.entity.UserGroup;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class UserGroupServiceTest {

    private UserGroupService userGroupService;

    @Before
    public void setUp() throws Exception {
        userGroupService = new UserGroupService();
    }

    @After
    public void tearDown() throws Exception {
        userGroupService = null;
    }

    @Test
    public void insertUserGroup() {
        UserGroup userGroup = new UserGroup();
        userGroup.setGroupName("group name");

        userGroupService.insertUserGroup(userGroup);
        Assert.assertTrue(userGroup.getId() != 0);

        UserGroup userGroupInsert = userGroupService.getUserGroupById(userGroup.getId());
        Assert.assertNotNull(userGroupInsert);
        Assert.assertEquals(userGroupInsert.getGroupName(), userGroup.getGroupName());
    }

    @Test
    public void getUserGroupById() {
        UserGroup userGroup = userGroupService.getUserGroupById(1);
        Assert.assertNotNull(userGroup);
    }

    @Test
    public void updateUserGroup() {
        UserGroup userGroup = userGroupService.getUserGroupById(3);
        userGroup.setGroupName("group name" + System.currentTimeMillis());

        userGroupService.updateUserGroup(userGroup);

        UserGroup userGroupUpdate = userGroupService.getUserGroupById(3);
        Assert.assertEquals(userGroupUpdate.getGroupName(), userGroup.getGroupName());
    }

    @Test
    public void deleteUserGroup() {
        UserGroup userGroupInsert = new UserGroup();
        userGroupInsert.setGroupName("group name");
        userGroupService.insertUserGroup(userGroupInsert);

        UserGroup userGroup = userGroupService.getUserGroupById(userGroupInsert.getId());
        userGroupService.deleteUserGroup(userGroup.getId());
        UserGroup userGroupDeleted = userGroupService.getUserGroupById(userGroupInsert.getId());
        Assert.assertNull(userGroupDeleted);
    }

    @Test
    public void getAllUserGroup() {
        List<UserGroup> userGroups = userGroupService.getAllUserGroup();
        Assert.assertNotNull(userGroups);
    }
}
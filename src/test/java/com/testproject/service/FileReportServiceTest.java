package com.testproject.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class FileReportServiceTest {

    @Test
    public void send() {
        FileReportService fileReportService = new FileReportService("vaseanicorici@gmail.com", "xls", new Date().toString());
        Assert.assertTrue(fileReportService.send());

        fileReportService = new FileReportService("vaseanicorici@gmail.com", null, new Date().toString());
        Assert.assertFalse(fileReportService.send());

        fileReportService = new FileReportService("vaseanicorici@gmail.com", "doc", null);
        Assert.assertTrue(fileReportService.send());

        fileReportService = new FileReportService("vaseanicorici@gmail.com", "xls2", new Date().toString());
        Assert.assertFalse(fileReportService.send());

        fileReportService = new FileReportService(null, "xls", new Date().toString());
        Assert.assertFalse(fileReportService.send());
    }
}
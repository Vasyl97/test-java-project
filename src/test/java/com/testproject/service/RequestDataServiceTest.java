package com.testproject.service;

import com.testproject.entity.RequestData;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class RequestDataServiceTest {

    private RequestDataService requestDataService;

    @Before
    public void setUp() throws Exception {
        requestDataService = new RequestDataService();
    }

    @After
    public void tearDown() throws Exception {
        requestDataService = null;
    }

    @Test
    public void getAllRequestData() {
        List<RequestData> requestData = requestDataService.getAllRequestData();
        Assert.assertNotNull(requestData);
    }

    @Test
    public void getAllRequestDataByDate() {
        List<RequestData> requestData = requestDataService.getAllRequestDataByDate(new Date().toString());
        Assert.assertNotNull(requestData);
    }
}
package com.testproject.service;

import com.testproject.entity.Country;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CountryServiceTest {

    private CountryService countryService;

    @Before
    public void setUp() throws Exception {
        countryService = new CountryService();
    }

    @After
    public void tearDown() throws Exception {
        countryService = null;
    }

    @Test
    public void insertCountry() {
        Country country = new Country();
        country.setCountryName("Country name");
        country.setLanguage("Country language");

        countryService.insertCountry(country);
        Assert.assertTrue(country.getId() != 0);

        Country countryInsert = countryService.getCountryById(country.getId());
        Assert.assertNotNull(countryInsert);
        Assert.assertEquals(countryInsert.getCountryName(), country.getCountryName());
        Assert.assertEquals(countryInsert.getLanguage(), country.getLanguage());
    }

    @Test
    public void getCountryById() {
        Country country = countryService.getCountryById(1);
        Assert.assertNotNull(country);
    }

    @Test
    public void updateCountry() {
        Country country = countryService.getCountryById(6);
        country.setCountryName("Country name" + System.currentTimeMillis());
        country.setLanguage("Country language" + System.currentTimeMillis());

        countryService.updateCountry(country);

        Country countryUpdate = countryService.getCountryById(6);
        Assert.assertEquals(countryUpdate.getCountryName(), country.getCountryName());
        Assert.assertEquals(countryUpdate.getLanguage(), country.getLanguage());
    }

    @Test
    public void deleteCountry() {
        Country countryInsert = new Country();
        countryInsert.setCountryName("Country test");
        countryInsert.setLanguage("Language test");
        countryService.insertCountry(countryInsert);

        Country country = countryService.getCountryById(countryInsert.getId());
        countryService.deleteCountry(country.getId());
        Country countryDeleted = countryService.getCountryById(countryInsert.getId());
        Assert.assertNull(countryDeleted);
    }

    @Test
    public void getAllCountries() {
        List<Country> countries = countryService.getAllCountries();
        Assert.assertNotNull(countries);
    }
}